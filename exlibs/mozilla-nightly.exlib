# Copyright 2017-2018 Rasmus Thomsen <cogitri@exherbo.org>
# Based in part upon 'mozilla-pgo.exlib', which is:
# Copyright 2007-2008 Alexander Færøy <eroyf@exherbo.org>
# Copyright 2008 Bernd Steinhauser <berniyh@exherbo.org>
# Copyright 2008, 2009 Ingmar Vanhassel <ingmar@exherbo.org>
# Copyright 2011 Brett Witherspoon <spoonb@exherbo.org>
# Copyright 2011, 2012, 2015 Wouter van Kesteren <woutershep@gmail.com>
# Distributed under the terms of the GNU General Public License v2

require freedesktop-desktop
require toolchain-funcs

myexparam co_project
exparam -v co_project co_project

export_exlib_phases src_prepare src_configure src_compile src_install

HOMEPAGE="http://www.mozilla.com/en-US/${PN}/"

SLOT="0"
MYOPTIONS="
    alsa
    bindist [[ description = [ Disable official branding, allowing binary redistribution ] ]]
    (
        debug [[ description = [ Disables optimization, enables assertions and other debug-only code ] ]]
    ) [[
        number-selected = at-most-one
        note = [ at-most-one because I felt it wasn't worth my time to figure out if it
                 was possible to build some kind of weird unoptimized-optimized monster.
                 It doesn't make much sense if you ask me, but patches are welcome. ]
    ]]
    dbus
    eme       [[ description = [ Enable Encrypted Media Extensions, a form of DRM used for sites like Netflix ] ]]
    gconf     [[ description = [ Use GConf for various preferences such as proxy, url-handlers, etc ] ]]
    hardening [[ description = [ Enable additional hardening flags, such as PIE ] ]]
    jack      [[ description = [ Enable JACK backend ] ]]
    libproxy  [[ description = [ Use libproxy for system proxy settings ] ]]
    pulseaudio
    startup-notification [[ description = [ Enables user and system feedback during startup ] ]]

    ( libc: musl )
    ( linguas: ${linguas[@]} )
    ( providers: jpeg-turbo )
"

DEPENDENCIES="
    build:
        app-arch/zip
        dev-lang/perl:*[>=5.6]
        dev-lang/python:2.7[sqlite]
        dev-lang/python:*[>=3]
        dev-lang/yasm[>=1.3]
        dev-python/pip[python_abis:2.7]
        sys-devel/autoconf:2.1
        virtual/pkg-config[>=0.9.0]
        virtual/unzip
    build+run:
        dev-lang/clang [[ note = [ required for stylo ] ]]
        dev-lang/rust:*[>=1.23]
        dev-libs/atk
        dev-libs/glib:2[>=2.26]
        dev-libs/libIDL:2[>=0.8.0]
        dev-libs/libevent:=
        dev-libs/libffi:=[>=3.0.10]
        media-libs/fontconfig[>=2.7.0]
        media-libs/freetype:2[>=2.1.0]
        media-libs/libvpx[>=1.5.0]
        x11-dri/mesa
        x11-libs/cairo[>=1.10.2-r1][X] [[ note = [ required for tee backend ] ]]
        x11-libs/gdk-pixbuf:2.0
        x11-libs/gtk+:3[>=3.4.0]
        x11-libs/libICE
        x11-libs/libX11
        x11-libs/libxcb
        x11-libs/libXcomposite
        x11-libs/libXdamage
        x11-libs/libXext
        x11-libs/libXfixes
        x11-libs/libXrender
        x11-libs/libXt
        x11-libs/pango[>=1.22.0]
        x11-libs/pixman:1[>=0.19.2]
        alsa? ( sys-sound/alsa-lib )
        dbus? ( sys-apps/dbus[>=0.60]
                dev-libs/dbus-glib:1[>=0.60] )
        gconf? ( gnome-platform/GConf:2[>=1.2.1] )
        jack? ( media-sound/jack-audio-connection-kit )
        libproxy? ( net-libs/libproxy:1[-webkit] ) [[ note = [ can't mix gtk3 and gtk2 symbols ] ]]
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
        pulseaudio? ( media-sound/pulseaudio )
        startup-notification? ( x11-libs/startup-notification[>=0.8] )
    suggestion:
        x11-libs/libnotify[>=0.4] [[ description = [ Show notifications with libnotify ] ]]
"

MOZILLA_SRC_CONFIGURE_PARAMS=(
    --disable-crashreporter
    --disable-system-sqlite
    --disable-updater
    --disable-warnings-as-errors
    --enable-content-sandbox
    --enable-default-toolkit=cairo-gtk3
    --enable-release # Additional optimizations
    --enable-sandbox
    --enable-startupcache
    --enable-webrtc
    --without-system-png # Requires patches for APNG support, which we will not support
    --with-system-bz2
    --with-system-ffi
    --with-system-libevent
    --with-system-libvpx
    --with-system-zlib
)

MOZILLA_SRC_CONFIGURE_OPTION_ENABLES=(
    dbus
    eme
    gconf
    hardening
    'hardening pie'
    jack
    libproxy
    pulseaudio
    startup-notification
)

MOZILLA_SRC_CONFIGURE_OPTION_WITHS=(
    # --with-system-jpeg needs libjpeg-turbo or whatever provides JCS_EXTENSIONS
    'providers:jpeg-turbo system-jpeg'
)

mozilla-nightly_src_prepare() {
    default

    edo sed \
        -e "/^includedir/ c includedir = /usr/$(exhost --target)/include/${PN}" \
        -e "/^idldir/ c idldir = /usr/share/idl/${PN}" \
        -e "/^installdir/ c installdir = /usr/$(exhost --target)/lib/${PN}" \
        -e "/^sdkdir/ c sdkdir = /usr/$(exhost --target)/lib/${PN}-devel" \
        -i config/baseconfig.mk
}

mozilla-nightly_src_configure() {
    # NOTE(somasis): fix library loading on musl
    # NOTE(woutershep): do not apply to glibc, breaks PGO.
    # also firefox 45.0 had rpath changes, this might be obsolete?
    libc-is-musl && append-flags "-Wl,-rpath=/usr/$(exhost --target)/lib/${PN}"

    export SHELL=/bin/bash

    local x

    # Needed to compile in a chroot environment
    export SHELL=/bin/bash

    local MOZILLA_AC_OPTIONS=(
        # econf can't be used because mozilla's build system is running the ./configure for us.
        # As a result we need to duplicate exherbo's defaults otherwise set by econf.
        # Removed from these defaults are datarootdir and docdir because they where --hates.
        --prefix=/usr/$(exhost --target)

        --disable-debug-symbols
        --disable-strip
        --enable-application=${co_project}
        --with-distribution-id=org.exherbo
        $(option_enable debug)

        # Disable tests, as these regularly break with nightly
        --disable-tests
    )

    local MOZILLA_MK_OPTIONS=(
        MOZ_CO_PROJECT="${co_project}"
        MOZ_MAKE_FLAGS="-j${EXJOBS}"
        MOZ_OBJDIR="${WORKBASE}"/build
        PKG_SKIP_STRIP=1
        TOOLCHAIN_PREFIX=$(exhost --tool-prefix)
    )

    # Override the default optimization levels of Mozilla projects with the
    # value specified by -O from the user's CFLAGS/CXXFLAGS (if -O isn't given
    # gcc defaults to '-O0'). Optimization must be disabled when the 'debug'
    # option is set. tests must be enabled for profiling against.
    if option debug; then
        MOZILLA_AC_OPTIONS+=( --enable-debug --disable-optimize --enable-tests )
    else
        local param_optimize
        for x in ${CFLAGS}; do
            case ${x} in
                -O)  param_optimize="--enable-optimize=-O1"  ;;
                -O*) param_optimize="--enable-optimize=${x}" ;;
            esac
        done
        [[ -z ${param_optimize} ]] && param_optimize="--enable-optimize=-O0"
        MOZILLA_AC_OPTIONS+=( --disable-debug "${param_optimize}" )
    fi

    if option bindist; then
        MOZILLA_AC_OPTIONS+=(
            --disable-official-branding
            # don't use --without-branding because it will result in an error
            --with-branding="${co_project}"/branding/unofficial
        )
    else
        MOZILLA_AC_OPTIONS+=( --enable-official-branding )
        MOZILLA_MK_OPTIONS+=( BUILD_OFFICIAL=1 MOZILLA_OFFICIAL=1 )
    fi

    # do this last so exhereses can override any above options
    MOZILLA_AC_OPTIONS+=(
        "${MOZILLA_SRC_CONFIGURE_PARAMS[@]}"
        $(for s in "${MOZILLA_SRC_CONFIGURE_OPTIONS[@]}"; do
            option ${s}
        done)
        $(for s in "${MOZILLA_SRC_CONFIGURE_OPTION_ENABLES[@]}"; do
            option_enable ${s}
        done)
        $(for s in "${MOZILLA_SRC_CONFIGURE_OPTION_WITHS[@]}"; do
            option_with ${s}
        done)
    )

    for x in "${MOZILLA_AC_OPTIONS[@]}"; do
        edo echo ac_add_options "${x}" >> .mozconfig
    done

    for x in "${MOZILLA_MK_OPTIONS[@]}"; do
        edo echo "export         ${x}" >> .mozconfig
        edo echo "mk_add_options ${x}" >> .mozconfig
    done
    if [[ ${PN} == thunderbird-nightly ]]; then
        edo ./mozilla/mach configure
    else
        edo ./mach configure
    fi
}

mozilla-nightly_src_compile() {
    if [[ ${PN} == thunderbird-nightly ]]; then
        edo ./mozilla/mach build
    else
        edo ./mach build
    fi
}

mozilla-nightly_src_install() {
    edo cd "${WORKBASE}/build"
    # Install manually, mach doesn't support custom DESTDIRS
    emake DESTDIR="${IMAGE}" install
    emagicdocs
}

