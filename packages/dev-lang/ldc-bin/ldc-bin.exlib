# Copyright 2018 Rasmus Thomsen <cogitri@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=ldc-developers ] bash-completion

export_exlib_phases pkg_setup src_prepare src_install

SUMMARY="The LLVM-based D Compiler."

LICENCES="
    Boost-1.0
    GPL-2
    UoI-NCSA
"
SLOT="0"

MYOPTIONS="
    platform: amd64
"

DOWNLOADS="
    platform:amd64? ( https://github.com/ldc-developers/ldc/releases/download/v${PV}/ldc2-${PV}-linux-x86_64.tar.xz )
"

DEPENDENCIES="
    run:
        dev-lang/llvm:*[>=3.7]
"

RESTRICT="mirror"

BASH_COMPLETIONS=( "etc/bash_completion.d/ldc2" )

ldc-bin_pkg_setup() {
    if option platform:amd64; then
        WORK="${WORKBASE}"/ldc2-${PV}-linux-x86_64
    fi
}

ldc-bin_src_prepare() {
    # Adjust paths to our file structure
    edo sed -e "s:-I%%ldcbinarypath%%/../import/ldc:-I/usr/$(exhost --target)/lib/${PN}/import/ldc:" \
        -i etc/ldc2.conf
    edo sed -e "s:-I%%ldcbinarypath%%/../import:-I/usr/$(exhost --target)/lib/${PN}/import:" \
        -i etc/ldc2.conf

    # We don't install lib32
    edo sed "/lib32/d" \
        -i etc/ldc2.conf
}

ldc-bin_src_install() {
    dobin bin/*
    dolib lib/*

    insinto /etc
    doins etc/ldc2.conf

    insinto /usr/$(exhost --target)/lib/${PN}
    doins -r import

    bash_completion_src_install
}

