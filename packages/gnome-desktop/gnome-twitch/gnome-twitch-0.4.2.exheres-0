# Copyright 2017-2018 Rasmus Thomsen <cogitri@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=vinszent tag=v${PV} ]
require gtk-icon-cache gsettings meson

SUMMARY="Enjoy Twitch on your GNU/Linux desktop"
HOMEPAGE="https://gnome-twitch.vinszent.com/"

LICENCES="GPL-3"
SLOT="0"
PLATFORMS="~amd64 ~x86"

MYOPTIONS="
    (
        gst-cairo
        gst-clutter
        gst-opengl
        mpv
    ) [[
    *description = [ Playback backend provider ]
    number-selected = at-least-one
    ]]
"

DEPENDENCIES="
    build+run:
        core/json-glib
        dev-libs/libpeas:1.0
        gnome-desktop/gobject-introspection:1
        gnome-desktop/libsoup:2.4
        net-libs/webkit:4.0
        x11-libs/gtk+:3
        gst-cairo? (
            media-libs/gstreamer:1.0
            media-plugins/gst-libav
            media-plugins/gst-plugins-bad:1.0
            media-plugins/gst-plugins-base:1.0
            media-plugins/gst-plugins-good:1.0
        )
        gst-clutter? (
            media-libs/clutter-gst:3.0
            media-libs/gstreamer:1.0
            media-plugins/gst-libav
            media-plugins/gst-plugins-base:1.0
            media-plugins/gst-plugins-bad:1.0
            media-plugins/gst-plugins-good:1.0
            x11-libs/clutter-gtk:1.0
        )
        gst-opengl? (
            media-libs/gstreamer:1.0
            media-plugins/gst-libav
            media-plugins/gst-plugins-base:1.0[gstreamer_plugins:opengl]
            media-plugins/gst-plugins-good:1.0
        )
        mpv? ( media/mpv )"

src_configure() {
    BACKENDS=( )

    option gst-cairo    && BACKENDS+=( gstreamer-cairo )
    option gst-clutter  && BACKENDS+=( gstreamer-clutter )
    option gst-opengl   && BACKENDS+=( gstreamer-opengl )
    option mpv          && BACKENDS+=( mpv-opengl )

    exmeson \
        -Dbuild-player-backends="$(IFS=, ; echo "${BACKENDS[*]}")" \
        --buildtype=release \
        ${MESON_SOURCE}
}

pkg_postrm() {
    gtk-icon-cache_pkg_postrm
    gsettings_pkg_postrm
}

pkg_postinst() {
    gtk-icon-cache_pkg_postinst
    gsettings_pkg_postinst
}

