# Copyright 2017 Rasmus Thomsen <Rasmus.thomsen@protonmail.com>
# Distributed under the terms of the GNU General Public License v2

SCM_REPOSITORY="https://gitlab.gnome.org/World/lollypop.git"
SCM_TAG="${PV}"
SCM_po_REPOSITORY="https://gitlab.gnome.org/World/lollypop-po.git"
SCM_SECONDARY_REPOSITORIES="po"
SCM_EXTERNAL_REFS="subprojects/po:po"

# Use scm-git.exlib instead of gitlab.exlib since the tarballs don't
# contain the subproject for the .po files
require scm-git
require python [ blacklist="2" multibuild=false python_opts="[sqlite]" ]
require gsettings gtk-icon-cache freedesktop-desktop
require meson

SUMMARY="Lollypop is a GNOME music playing application"
LICENCES="GPL-3"

PLATFORMS="~amd64 ~x86"
SLOT="0"

# Upstream doesn't check for all dependencies and only mentions them in the README
# ...
DEPENDENCIES="
    build:
        dev-util/itstool
        dev-util/intltool
        virtual/pkg-config
    build+run:
        dev-libs/appstream-glib
        dev-libs/glib:2
        dev-python/pycairo[python_abis:*(-)?]
        dev-python/dbus-python[python_abis:*(-)?]
        dev-util/desktop-file-utils
        gnome-bindings/pygobject:3[python_abis:*(-)?][cairo]
        gnome-desktop/gobject-introspection:1[>=1.35.9][python_abis:*(-)?]
        gnome-desktop/totem-pl-parser
        media-libs/gstreamer:1.0[gobject-introspection]
        x11-libs/gtk+:3[>=3.20]
    run:
        dev-python/pylast[>=1.0][python_abis:*(-)?]
    recommendation:
        media-plugins/gst-libav [[ description = [ Required to play many music formats,
            like mp3 and flac ] ]]
    suggestion:
        dev-python/lollypop-portal [[ description = [ Adds additional functionality to
            lollypop, like control over pulseaudio via pa-cmd ] ]]
"

# Appdata validation needs to download files for testing
RESTRICT="test"

src_prepare() {
    meson_src_prepare

    # use correct python libdir
    edo sed "/python.sysconfig_path('purelib'))/d" -i \
        meson.build
    edo sed -e "20s:^:python_dir = '$(python_get_libdir)'\n:" -i \
        meson.build
}

src_install() {
    meson_src_install

    keepdir /usr/$(exhost --target)/lib/python$(python_get_abi)/lollypop/thirdparty
}

pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    gtk-icon-cache_pkg_postrm
    gsettings_pkg_postrm
}

pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    gtk-icon-cache_pkg_postinst
    gsettings_pkg_postinst
}

